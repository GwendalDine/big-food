CREATE TABLE `recipe` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `recipe` (`id`, `name`, `description`, `price`, `image_id`, `type_id`) VALUES
(1, 'Hamburger', 'This is a Hamburger', 7.99, 3, 1);

SELECT * FROM `restaurant` JOIN `user` ON `restaurant`.`manager_id` = `user`.`id` WHERE `user`.`username` = 'Gwendal';