<?php

namespace BigFoodBundle\Controller;

use OrderBundle\Entity\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BigFoodController extends Controller
{
    /**
     * Displays the homepage
     */
    public function indexAction()
    {
        return $this->render('BigFoodBundle::layout.html.twig');
    }

    /**
     * Get the amount of items in the cart, to be displayed in the navbar
     */
    public function cartNumberAction()
    {
        $em = $this->getDoctrine()->getManager();

        $count = 0;

        // If the user is currently building a cart (cart status 1)
        if($cart = $em->getRepository(Cart::class)->findOneBy(['owner' => $this->getUser(), 'status' => 1])){
            $count = count($cart->getRecipes());
        }

        return $this->render('BigFoodBundle::cart.html.twig', array(
            'count' => $count
        ));
    }
}
