<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Restaurant;
use RestaurantBundle\Form\RestaurantType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class RestaurantController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     * Creates a new restaurant
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // Create an empty restaurant and the form to hydrate it
        $restaurant = new Restaurant();
        $form = $this->createForm(RestaurantType::class, $restaurant);

        // If the form was submited and is valid, persist the hydrated restaurant in the database
        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()){
            $em->persist($restaurant);
            $em->flush();

            $this->addFlash('success','New restaurant created');

            return $this->redirectToRoute('view_restaurant', array(
                'id' => $restaurant->getId(),
                'name' => $restaurant->getName()
            ));
        }

        return $this->render('RestaurantBundle::create_restaurant.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Displays a restaurant's page
     */
    public function viewAction(Restaurant $restaurant)
    {
        return $this->render('RestaurantBundle::view_restaurant.html.twig', array(
            'restaurant' => $restaurant
        ));
    }

    /**
     * Displays a list of restaurant in the nav bar
     */
    public function restaurantsListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $restaurants = $em->getRepository('RestaurantBundle:Restaurant')->findAll();

        return $this->render('RestaurantBundle::restaurants_list.html.twig', array(
            'restaurants' => $restaurants
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     * Adds/Changes a restaurant's manager
     */
    public function setManagerAction(Restaurant $restaurant, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findAll();

        if($request->isMethod('POST')){

            $userId = $request->request->get('manager');
            $user = $em->getRepository('UserBundle:User')->find($userId);

            // adds the global role ROLE_MANAGER to the user
            $user->addRole('ROLE_MANAGER');
            // sets him as the restaurant's manager
            $restaurant->setManager($user);
            $em->persist($restaurant);
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', $user->getUsername() . ' has been set as ' . $restaurant->getName() . '\'s manager.');

            return $this->redirectToRoute('view_restaurant', array(
                'name'  => $restaurant->getName(),
                'id'    => $restaurant->getId()
            ));
        }

        return $this->render('RestaurantBundle::set_manager.html.twig', array(
            'restaurant' => $restaurant,
            'users'      => $users
        ));
    }
}
