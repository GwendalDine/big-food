<?php

namespace RestaurantBundle\Controller;


use RestaurantBundle\Entity\Type;
use RestaurantBundle\Form\TypeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class TypeController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     * Adds a new recipe type to the dabatase
     */
    public function addRecipeTypeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // Create an empty type and the form to hydrateit
        $type = new Type();
        $form = $this->createForm(TypeType::class, $type);

        // If the form was submited and is valid, persist the hydrated type in the database
        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()){
            $em->persist($type);
            $em->flush();

            $this->addFlash('success', 'Recipe type added');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('RestaurantBundle::add_recipe_type.html.twig', array(
            'form' => $form->createView()
        ));
    }
}