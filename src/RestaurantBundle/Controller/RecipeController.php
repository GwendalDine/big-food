<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Recipe;
use RestaurantBundle\Entity\Restaurant;
use RestaurantBundle\Form\RecipeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class RecipeController extends Controller
{
    /**
     * @Security("has_role('ROLE_MANAGER')")
     *
     * Adds a recipe to the database
     */
    public function createRecipeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // Creates an empty recipe
        $recipe = new Recipe();

        // Creates a form to hydrate the recipe
        $form = $this->createForm(RecipeType::class, $recipe);

        // If the form was submited and valid, persist the recipe in the database
        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()){
            $em->persist($recipe);
            $em->flush();

            $this->addFlash('success', 'Recipe created');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('RestaurantBundle::create_recipe.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Security("has_role('ROLE_MANAGER')")
     *
     * Displays every recipe to a manager
     */
    public function manageRecipeAction()
    {
        $em = $this->getDoctrine()->getManager();

        $recipes = $em->getRepository('RestaurantBundle:Recipe')->findBy([], ['type' => 'ASC']);

        return $this->render('RestaurantBundle::manage_recipe.html.twig', array(
            'recipes'    => $recipes
        ));
    }

    /**
     * @Security("has_role('ROLE_MANAGER')")
     *
     * Edit a recipe's informations
     */
    public function editRecipeAction(Recipe $recipe, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(RecipeType::class, $recipe);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()){
            $em->persist($recipe);
            $em->flush();

            $this->addFlash('success', $recipe->getName() . ' edited');

            return $this->redirectToRoute('manage_recipe');
        }

        return $this->render('RestaurantBundle::edit_recipe.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Security("has_role('ROLE_MANAGER')")
     *
     * Delete a recipe from the database
     */
    public function deleteRecipeAction(Recipe $recipe)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($recipe);
        $em->flush();

        $this->addFlash('warning', $recipe->getName() . ' deleted');

        return $this->redirectToRoute('manage_recipe');
    }
}
