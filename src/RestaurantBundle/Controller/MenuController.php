<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Recipe;
use RestaurantBundle\Entity\Restaurant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends Controller
{
    /**
     * Displays a restaurant's menu
     */
    public function viewMenuAction(Restaurant $restaurant, $type)
    {
       $em = $this->getDoctrine()->getManager();

       // If the user wants to see every type of recipe, store each one in their respective array
       if ($type == 'all'){
           $burgers = $em->getRepository(Recipe::class)->findRecipesByType('Burgers');
           $drinks = $em->getRepository(Recipe::class)->findRecipesByType('Drinks');
           $desserts = $em->getRepository(Recipe::class)->findRecipesByType('Desserts');
           return $this->render('RestaurantBundle::view_restaurant_menu.html.twig', array(
               'restaurant' => $restaurant,
               'burgers'    => $burgers,
               'drinks'     => $drinks,
               'desserts'   => $desserts
           ));

       }else{
           // Just fetch the one type
           $recipes = $em->getRepository(Recipe::class)->findRecipesByType($type);
           return $this->render('RestaurantBundle::view_restaurant_menu.html.twig', array(
               'type'        => $type,
               'restaurant'  => $restaurant,
               'recipes'     => $recipes
           ));
       }
    }

    /**
     * @Security("has_role('ROLE_MANAGER')")
     *
     * Displays every recipe to a restaurant's manager
     */
    public function manageMenuAction(Restaurant $restaurant)
    {
        // If the user is the restaurant's manager
        if($restaurant->getManager()->getId() == $this->getUser()->getId()){
            $em = $this->getDoctrine()->getManager();

            $recipes = $em->getRepository('RestaurantBundle:Recipe')->findBy([], ['type' => 'ASC']);

            return $this->render('RestaurantBundle::manage_menu.html.twig', array(
                'restaurant' => $restaurant,
                'recipes'    => $recipes
            ));
        } else {
            return new Response('You are not this restaurant\'s manager');
        }
    }

    /**
     * @Security("has_role('ROLE_MANAGER')")
     * @ParamConverter("restaurant", options={"mapping": {"restaurant": "id"}})
     * @ParamConverter("recipe", options={"mapping": {"recipe": "id"}})
     *
     * Allows the restaurant's manager to add an existing recipe to his restaurant
     */
    public function addRecipeToMenuAction(Restaurant $restaurant, Recipe $recipe)
    {
        // If the user is the restaurant's manager
        if($restaurant->getManager()->getId() == $this->getUser()->getId()) {
            $em = $this->getDoctrine()->getManager();
            $restaurant->addRecipe($recipe);
            $em->persist($restaurant);
            $em->flush();

            return $this->redirectToRoute('manage_menu',array(
                    'id' => $restaurant->getId()
            ));
        }else {

            return new Response('nope');
        }
    }

    /**
     * @Security("has_role('ROLE_MANAGER')")
     * @ParamConverter("restaurant", options={"mapping": {"restaurant": "id"}})
     * @ParamConverter("recipe", options={"mapping": {"recipe": "id"}})
     *
     * Allows the restaurant's manager to remove a recipe from his restaurant
     */
    public function removeRecipeFromMenuAction(Restaurant $restaurant, Recipe $recipe)
    {
        // If the user is the restaurant's manager
        if($restaurant->getManager()->getId() == $this->getUser()->getId()) {
            $em = $this->getDoctrine()->getManager();
            $restaurant->removeRecipe($recipe);
            $em->persist($restaurant);
            $em->flush();

            return $this->redirectToRoute('manage_menu', array(
                'id' => $restaurant->getId()
            ));
        }else {

            return new Response('nope');
        }
    }
}
