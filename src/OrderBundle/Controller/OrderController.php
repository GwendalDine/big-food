<?php

namespace OrderBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use OrderBundle\Entity\Cart;
use RestaurantBundle\Entity\Recipe;
use RestaurantBundle\Entity\Restaurant;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OrderController extends Controller
{
    /**
     * @Security("has_role('ROLE_MANAGER')")
     *
     * Displays all pending orders (cart status 2) for the manager
     */
    public function manageOrdersAction()
    {
        $em = $this->getDoctrine()->getManager();

        // If the user is the manager of a restaurant
        if($restaurant = $em->getRepository(Restaurant::class)->findOneBy(['manager' => $this->getUser()])){
            // Find every orders that belong to this restaurant and are pending
            $carts = $em->getRepository(Cart::class)->findBy(['status' => 2, 'restaurant' => $restaurant]);

            return $this->render('OrderBundle::manage_orders.html.twig', array(
                'restaurant' => $restaurant,
                'carts'       => $carts
            ));
        }else{
            $this->addFlash('danger', 'You are not a manager for any restaurant');

            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @ParamConverter("recipe", options={"mapping": {"recipe": "id"}})
     * @ParamConverter("restaurant", options={"mapping": {"restaurant": "id"}})
     */
    public function addRecipeToCartAction(Recipe $recipe, Restaurant $restaurant)
    {
        $em = $this->getDoctrine()->getManager();

        // Check whether or not the user is currently building his cart (cart status 1) and creates one if he's not
        if (!($cart = $em->getRepository(Cart::class)->findOneBy(['owner' => $this->getUser(), 'status' => 1]))) {
            $cart = new Cart();
            $cart->setStatus(1);
            $cart->setOwner($this->getUser());
        }

        // If the user already started ordering from one restaurant
        if ($cart->getRestaurant() != null) {

            // Check to make sure the user isn't trying to order from different restaurant
            if($cart->getRestaurant()->getId() != $restaurant->getId()){
                $this->addFlash('warning', 'You cannot add recipes from different Big Food shops in your cart at the same time');

                return $this->redirectToRoute('homepage');
            }

            // If the restaurant the user has been building his cart from has the recipe he's trying to add
            if ($cart->getRestaurant()->hasRecipe($recipe)) {
                $cart->addRecipe($recipe);
            }
        }else{
            // Add a restaurant to newly started cart
            $cart->setRestaurant($restaurant);
            $cart->addRecipe($recipe);
        }

        $em->persist($cart);
        $em->flush();

        $this->addFlash('success', $recipe->getName() . ' added to your cart !');
        return $this->redirectToRoute('view_menu', array(
            'id'    => $restaurant->getId(),
            'type'  => 'all'
        ));
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * Displays the cart to the user
     */
    public function checkoutAction()
    {
        $em = $this->getDoctrine()->getManager();

        // If the user has been building a cart
        if($cart = $em->getRepository(Cart::class)->findOneBy(['owner' => $this->getUser(), 'status' => 1])){

            return $this->render('OrderBundle::checkout.html.twig', array(
                'cart' => $cart
            ));
        }else{

            return $this->render('OrderBundle::checkout.html.twig');
        }
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @ParamConverter("recipe", options={"mapping": {"recipe": "id"}})
     * @ParamConverter("cart", options={"mapping": {"cart": "id"}})
     */
    public function removeFromCartAction(Recipe $recipe, Cart $cart)
    {
        $em = $this->getDoctrine()->getManager();

        // If the user is the owner of the cart
        if($cart->getOwner()->getId() == $this->getUser()->getId()){
            // Remove a recipe from it
            $cart->removeRecipe($recipe);

            // If the recipe being removed was the last from the cart
            if(count($cart->getRecipes()) == 0) {
                // Delete the cart
                $em->remove($cart);
                $em->flush();

                $this->addFlash('info',  $recipe->getName() . ' removed.');

                return $this->redirectToRoute('homepage');
            }

            $em->persist($cart);
            $em->flush();
        } else {

            return new Response('Nope');
        }
        $this->addFlash('info',  $recipe->getName() . ' removed.');

        return $this->redirectToRoute('checkout');
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * Changes the cart status to pending (2), sent to the restaurant manager
     */
    public function proceedWithCheckoutAction(Cart $cart)
    {
        $cart->setStatus(2);
        $cart->setDate(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($cart);
        $em->flush();

        $this->addFlash('success', 'Your order has been sent to your restaurant, it\'ll be ready in 30 minutes');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Security("has_role('ROLE_MANAGER')")
     *
     * Changes the cart status to archived (3), the customer came and picked it up.
     */
    public function archiveOrderAction(Cart $cart)
    {
        // If the user is the restaurant's manager
        if($cart->getRestaurant()->getManager()->getId() == $this->getUser()->getId()) {
            // Archives the cart
            $cart->setStatus(3);

            $em = $this->getDoctrine()->getManager();
            $em->persist($cart);
            $em->flush();

            $this->addFlash('info', 'This order has been archived');

            return $this->redirectToRoute('manage_orders');
        } else {

            return new Response('nope');
        }
    }
}
