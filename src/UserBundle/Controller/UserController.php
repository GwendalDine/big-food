<?php

namespace UserBundle\Controller;

use OrderBundle\Entity\Cart;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * Displays his pending (status 2) and archived (status 3) orders to a user
     */
    public function showOrdersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $pendingCarts = $em->getRepository(Cart::class)->findBy(['owner' => $this->getUser(), 'status' => 2]);
        $archivedCarts = $em->getRepository(Cart::class)->findBy(['owner' => $this->getUser(), 'status' => 3]);

        return $this->render('UserBundle::show_orders.html.twig', array(
            'pending'   => $pendingCarts,
            'archived'  => $archivedCarts
        ));
    }
}
